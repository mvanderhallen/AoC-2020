{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DataKinds, KindSignatures #-}

-- import Data.Maybe
import Data.Either (lefts, rights)
import Data.List (transpose, intersect, sortOn, union, (\\))
import Data.List.Index

data Range = MkRange { name :: String, ll :: Int, lu :: Int, rl :: Int, ru :: Int}-- left lower, left upper, right lower, right upper
  deriving (Show)

instance Eq Range where
  (MkRange n _ _ _ _) == (MkRange n' _ _ _ _) = n == n'

data Validity = Valid | Invalid | Unchecked

newtype Ticket (a::Validity) = MkTicket { unTicket :: [Int]}
  deriving stock (Show)
  deriving newtype (Eq, Ord)

unchecked :: [Int] -> Ticket 'Unchecked
unchecked = MkTicket

-- Lets forgo parsing because the format is unhandy and yet so easy to translate
ranges :: [Range]
ranges =
  [ MkRange "departure location" 36 626 651 973
  , MkRange "departure station" 38 134 142 966
  , MkRange "departure platform" 32 465 489 972
  , MkRange "departure track" 40 420 446 973
  , MkRange "departure date" 38 724 738 961
  , MkRange "departure time" 30 358 377 971
  , MkRange "arrival location" 48 154 166 965
  , MkRange "arrival station" 48 669 675 968
  , MkRange "arrival platform" 27 255 276 965
  , MkRange "arrival track" 37 700 720 955
  , MkRange "class" 50 319 332 958
  , MkRange "duration" 35 822 835 949
  , MkRange "price" 40 791 802 951
  , MkRange "route" 42 56 82 968
  , MkRange "row" 40 531 555 968
  , MkRange "seat" 49 681 695 962
  , MkRange "train" 31 567 593 953
  , MkRange "type" 42 840 855 949
  , MkRange "wagon" 31 165 176 962
  , MkRange "zone" 48 870 896 970
  ]

rangesExample :: [Range]
rangesExample =
  [ MkRange "class" 1 3 5 7
  , MkRange "row" 6 11 33 44
  , MkRange "seat" 13 40 45 50
  ]

myTicket :: Ticket 'Valid
myTicket = MkTicket [127,89,149,113,181,131,53,199,103,107,97,179,109,193,151,83,197,101,211,191]

getFieldAt :: Ticket a -> Int -> Int
getFieldAt = (!!) . unTicket

checkValid :: Int -> Range -> Bool
checkValid i (MkRange {..}) = (ll <= i && i <= lu) || (rl <= i && i <= ru)

checkValid' :: [Range] -> Int -> Bool
checkValid' rs i = any (checkValid i) rs

checkTicket :: [Range] -> Ticket a -> Either (Ticket 'Valid) (Ticket 'Invalid)
checkTicket rs is = case filter (not . checkValid' rs) $ unTicket is of
  []      -> Left $ MkTicket $ unTicket is
  invalid -> Right $ MkTicket $ invalid

input :: FilePath -> IO [[Int]]
input fp = do
  ip <- lines <$> readFile fp
  return $ map (\x -> read $ "[" ++ x ++ "]") ip

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  tickets <- fmap unchecked <$> input fp
  let invalid = rights $ map (checkTicket ranges) tickets
  print $ sum $ concatMap (unTicket) invalid

-------------- Assignment 2 ---------------------------

rangesExample2 :: [Range]
rangesExample2 =
  [ MkRange "class" 0 1 4 19
  , MkRange "row" 0 5 8 19
  , MkRange "seat" 0 13 16 19
  ]

validFor :: [Range] -> Int -> [Range]
validFor rs i = filter (checkValid i) rs

possibleParseTicket :: [Range] -> [Int] -> [[Range]]
possibleParseTicket rs = map (validFor rs)

departure_fields :: [Range]
departure_fields = [
    MkRange "departure location" 36 626 651 973
  , MkRange "departure station" 38 134 142 966
  , MkRange "departure platform" 32 465 489 972
  , MkRange "departure track" 40 420 446 973
  , MkRange "departure date" 38 724 738 961
  , MkRange "departure time" 30 358 377 971
  ]

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  let ranges' = ranges
  tickets <- fmap unchecked <$> input fp
  let valid = lefts $ map (checkTicket ranges') tickets
  let transvalid = transpose $ map unTicket valid
  let possibleFields = sortOn (length . snd) $ [(fn,foldl1 intersect validFors) | (fn, fieldvals) <- indexed transvalid, let validFors = map (validFor ranges') fieldvals]
  let fieldPositions = assign possibleFields
  let departurePositions = map fst $ filter ((`elem` departure_fields) . snd) fieldPositions
  let magicNumber = product $ map (myTicket `getFieldAt`) $ departurePositions
  print magicNumber



assign :: [(Int, [Range])] -> [(Int, Range)]
assign irs = snd $ foldl go ([], []) irs
  where
    go :: ([Range], [(Int, Range)]) -> (Int, [Range]) -> ([Range], [(Int, Range)])
    go (used, acc) (i, rs) = case (rs \\ used) of
      [r]       -> (r:used, (i,r):acc)
      otherwise -> error $ show (i,rs)

main :: IO ()
main = do
  fp <- getLine
  assignment1 fp
  assignment2 fp
