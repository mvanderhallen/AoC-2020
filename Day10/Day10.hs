{-# LANGUAGE BangPatterns #-}


module Day10 where

import Data.List (sort, tails, break, group)
import Debug.Trace
import Control.Arrow

input :: FilePath -> IO [Int]
input fp = (sort . map read . lines) <$> readFile fp

differences :: Num a => [a] -> [a]
differences xs = (3:) $ zipWith (-) xs (0:xs)

freqTable :: Ord a => [a] -> [(a,Int)]
freqTable = map (head &&& length) . group . sort

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  inp <- sort <$> input fp
  print inp
  print $ differences inp
  let x = freqTable $ differences inp
  print $  x

findJumps' :: [Int] -> [[Int]]
findJumps' (a:as) = findJumps'_helper as a

findJumps'_helper :: [Int] -> Int -> [[Int]]
findJumps'_helper [] _ = [[]]
findJumps'_helper adapters base= do
  let (poss, t) = span ((\x -> x <= 3) . flip (-) base) adapters
  if null poss then
    []
  else
    [x : sol | (x:xs) <- tails poss, sol <-findJumps'_helper (xs ++ t) x]

breakGroups :: [Int] -> [[Int]]
breakGroups []   = [[]]
breakGroups [a'] = [[a']]
breakGroups (a:a':as)
  | a' - a == 3 = [a]:(breakGroups (a':as))
  | otherwise   = let (h:t) = breakGroups (a':as) in (a:h):t

breakGroups' :: [Int] -> [[Int]]
breakGroups' x = map (map fst) . breaks ((== 3) . uncurry (-)) $ zip x (head x : x)

breaks :: (a -> Bool) -> [a] -> [[a]]
breaks p = reverse . map (reverse) . foldl (\(a:acc) e -> if p e then [e]:a:acc else (e:a):acc) [[]]

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  inp <- input fp
  print $ combinations True inp
  -- Quite brute force
  -- print $ product $ map (length . findJumps') $ breakGroups (0:inp)


combinations :: Bool -> [Int] -> Int
combinations flag = flip (combinations' flag) 0 . (0:)

-- Memoization using open recursion. Note that it is very important for performance that the memoization function is eta-reduced!
combinations' :: Bool -> [Int] -> Int -> Int
combinations' flag !list !index = memoization_func index
  where
    l :: Int
    l = length list
    combinations_, combinationsMemo, combinationsMemo_eta, memoization_func :: Int -> Int
    memoization_func index = if flag then combinationsMemo_eta index else combinationsMemo index
    combinationsMemo_eta   = (map (\i -> combinations_ i) [0..] !!)
    combinationsMemo index = (map (\i -> combinations_ i) [0..] !!) index
    combinations_ index =
      case compare index (l-1) of
        GT    -> id $! 0
        EQ    -> id $! 1
        LT    -> id $! recres
          where
            recres = sum [ memoization_func index' | !index'<-[(index+1)..(index+3)], index' < l, list!!index' - list!!index <= 3]
