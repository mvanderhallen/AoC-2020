import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Text.Parsec.Expr
import Data.Either
import Control.Monad.Identity (Identity)

type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace
parens = P.parens lexer

data ExprF = MkMult ExprF ExprF | MkPlus ExprF ExprF | MkNat Integer deriving (Show, Eq)

expr :: Parser ExprF
expr    = buildExpressionParser table term
        <?> "expression"

term :: Parser ExprF
term =  parens expr
    <|> MkNat <$> natural
    <?> "simple expression"

table :: [[Operator String u Identity ExprF]]
table = [[binary "*" MkMult AssocLeft, binary "+" MkPlus AssocLeft]]

binary :: String -> (a -> a -> a) -> Assoc -> Operator String u Identity a
binary  name fun assoc = Infix (do{ P.reservedOp lexer name; return fun }) assoc

input :: FilePath -> Parser ExprF -> IO [ExprF]
input fp pExpr = do
  ip <- readFile fp
  return $ rights $ map (\n -> parse pExpr fp n) $ lines ip

val :: ExprF -> Integer
val (MkMult e1 e2) = (val e1) * (val e2)
val (MkPlus e1 e2) = (val e1) + (val e2)
val (MkNat e1)     = e1

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  ip <- input fp expr
  print $ sum $ map val ip

------ ASSIGNMENT 2 -------
expr' :: Parser ExprF
expr' = buildExpressionParser table' term' <?> "expression'"

term' :: Parser ExprF
term' =  parens expr'
     <|> MkNat <$> natural
     <?> "simple expression'"

table' :: [[Operator String u Identity ExprF]]
table' = [ [binary "+" MkPlus AssocLeft]
         , [binary "*" MkMult AssocLeft]
         ]

assignment2 :: FilePath -> IO ()
assignment2 fp = do
 ip <- input fp expr'
 print $ sum $ map val ip

main :: IO ()
main = do
  putStr "Process file at: "
  fp <- getLine
  assignment1 fp
  assignment2 fp
