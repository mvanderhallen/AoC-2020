{-# LANGUAGE BangPatterns #-}

import Data.IntMap.Strict

-- assignment1  :: IO ()

assignment1 :: IO ()
assignment1 = solve [9,12,1,4,17,0,18] 0 2020

assignment2 :: IO ()
assignment2 = solve [9,12,1,4,17,0,18] 0 30000000

solve :: [Int] -> Int -> Int -> IO ()
solve list i n= do
  let hm = fromList $ zip list [1..]
  print $ turn (1+length list) hm (last list) i
  where
    turn :: Int -> IntMap Int -> Int -> Int -> Int
    -- turn n _  !i !j = j
    turn !x im !i !j
      | x == n    = j
      | otherwise = let !new = x - (findWithDefault x j im) in turn (x+1) (insert j x im) j new

main = do
  assignment1
  assignment2
