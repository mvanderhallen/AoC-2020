# Advent Of Code 2020
This repository contains my solutions, input and testfiles for the advent of code edition 2020.
It is all written in Haskell.

## Running in REPL
The following command should work on most `.hs` files, most days limit themselves to these packages:
```
ghci -package parsec -package unordered-containers -package hashable ******
```

## Compiling

The following command should work on many `.hs` files, although some might not have a `main` function:
```
ghc -package parsec -package unordered-containers -package hashable -O3 *****
```

![](AdventOfCode.png)
