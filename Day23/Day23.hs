{-# LANGUAGE BangPatterns #-}

import Prelude hiding (pred, succ)

import Control.DeepSeq
import qualified Data.IntMap.Strict as IM
import Data.Composition ((.:))
import System.IO (hFlush, stdout)

data DoublyLinkedItem = DLI { pred :: Int, succ::Int } deriving (Show, Eq)
type List = IM.IntMap DoublyLinkedItem
type ListView = (List, Int)

succOf :: List -> Int -> Int
succOf = succ .: (IM.!)

takeCW' :: List -> Int -> ([Int], List)
takeCW' im i = ([t1,t2,t3], IM.adjust (\a -> a {succ = t4}) i $ IM.adjust (\a -> a {pred = i}) t4 im)
  where
    t1 = succOf im i
    t2 = succOf im t1
    t3 = succOf im t2
    t4 = succOf im t3

putCW' :: List -> [Int] -> Int -> List
putCW' im ([v1,v2,v3]) i = IM.adjust (\a -> a {succ = t1}) v3 $ IM.adjust (\a -> a {pred = v3}) t1 $ IM.adjust (\a -> a { succ = v1 }) i im
  where
    t1 = succOf im i

rounds :: Int -> Int -> ListView -> ListView
rounds _ 0 v = v
rounds m !n !v = rounds m (n-1) $! Main.round m v

round :: Int -> ListView -> ListView
round m (im, i) = (putCW' im' taken sel'', i')
  where
    (!taken, im') = takeCW' im i
    oneDown x = case (x-1) `mod` m of
      0 -> m
      v -> v
    !sel' = (oneDown i) : map (oneDown) sel'
    !sel'' = head $ dropWhile (`elem` taken) sel'
    !i' = succOf im' i

toListView :: [Int] -> ListView
toListView l = (IM.fromList $ zipWith3 (\x y z -> (x, DLI y z)) l (last l : l) (drop 1 l ++ [head l]), head l)

input_part1, input_part2 :: ListView
input_part1 = toListView $ [7,8,9,4,6,5,1,2,3]
input_part2 = toListView $ [7,8,9,4,6,5,1,2,3] ++ [10..1000000]

test_part1, test_part2 :: ListView
test_part1 = toListView $ [3,8,9,1,2,5,4,6,7]
test_part2 = toListView $ [3,8,9,1,2,5,4,6,7] ++ [10..1000000]


clockwiseFrom :: Int -> List -> [Int]
clockwiseFrom i im = i : (map go $ clockwiseFrom i im)
  where
    go = succOf im

assignment1 :: Bool -> IO ()
assignment1 b = do
  let input = if b then input_part1 else test_part1
  print $ take 9 $ clockwiseFrom 1 $ fst $ rounds 9 100 input

assignment2 :: Bool -> IO ()
assignment2 b = do
  let input = if b then input_part2 else test_part2
  print $ product $ take 3 $ clockwiseFrom 1 $ fst $ rounds 1000000 10000000 input


main :: IO ()
main = do
  putStr "Input [y] or test [n]: "
  hFlush stdout
  a <- getLine
  if a == "y" then do
    assignment1 True
    assignment2 True
  else do
    assignment1 False
    assignment2 False
