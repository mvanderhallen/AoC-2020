module Day5 where
  import Data.List ((\\))
  type Seat = Int

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  parse :: String -> (Int,Int)
  parse s = (row,col)
    where
      (row,_) = parseRow' 0 127 (take 7 s)
      (col,_) = parseSeat' 0 7 (drop 7 s)

  parseRow' l u [] = (l,u)
  parseRow' l u ('F':t)= parseRow' l (newBound l u) t
  parseRow' l u ('B':t)= parseRow' (1+newBound l u) u t


  parseSeat' l u [] = (l,u)
  parseSeat' l u ('L':t)= parseSeat' l (newBound l u) t
  parseSeat' l u ('R':t)= parseSeat' (1+newBound l u) u t

  newBound l u = (l+(u-l) `div` 2)

  toSeatId :: (Int, Int) -> Int
  toSeatId (row, col) = row * 8 + col

  assignment1 :: IO ()
  assignment1 = do
    seats <- input
    let seatIds = map (toSeatId . parse) seats
    print seatIds
    print $ maximum seatIds

  assignment2 :: IO ()
  assignment2 = do
    seats <- input
    let seatIds = map (toSeatId . parse) seats
    let start   = minimum seatIds
    print $ head $ [start..] \\ seatIds
