{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Text.Parsec (many1, many, sepBy1, Parsec, parse, char, letter, string)
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import qualified Data.HashMap.Strict as HM

import Data.Hashable (Hashable)
import Control.Arrow (second)

import Data.List (intersect, (\\), sortOn, delete)

type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace
parens = P.parens lexer

newtype Ingredient = MkIngredient { unIngredient :: String } deriving newtype (Eq, Ord, Hashable)
newtype Allergen = MkAllergen { unAllergen :: String } deriving newtype (Eq, Ord, Hashable)
data Rule = MkRule [Ingredient] [Allergen] deriving Show

instance Show Allergen where
  show = unAllergen

instance Show Ingredient where
  show = unIngredient

pIngredient :: Parser Ingredient
pIngredient = MkIngredient <$> lexeme (many1 letter)

pAllergen :: Parser Allergen
pAllergen = MkAllergen <$> lexeme (many1 letter)

pComma :: Parser Char
pComma = lexeme $ char ','

pRule :: Parser Rule
pRule = lexeme $ do
  ingredients <- many1 pIngredient
  allergens <- parens ((lexeme $ string "contains") *> pAllergen `sepBy1` pComma)
  return $ MkRule ingredients allergens

input :: FilePath -> IO [Rule]
input fp = do
  ip <- readFile fp
  case parse (many1 pRule) fp ip of
    Left err -> error $ show err
    Right rules -> return rules

type AllergenMap = HM.HashMap Allergen [Ingredient]
type IngredientCount = HM.HashMap Ingredient Int

construct :: [Rule] -> (IngredientCount, AllergenMap)
construct rules = foldl go (HM.empty, HM.empty) rules
  where
    go :: (IngredientCount, AllergenMap) -> Rule -> (IngredientCount, AllergenMap)
    go (ing, allergenMap) (MkRule i al) = (nIng, allergenMap')
      where
        nIng = foldr(\al -> HM.insertWith (+) al 1) ing i --ing `union` i
        allergenMap' = foldr (\al -> HM.insertWith (intersect) al i) allergenMap al

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  rules <- input fp
  let (ic, am) = construct rules
  let unused = (HM.keys ic) \\  (concat (HM.elems am))
  print $ sum $ [ic HM.! ing | ing <- unused]

---------- ASSIGNMENT 2 ----------

resolve :: [(Allergen, [Ingredient])] -> [(Allergen, Ingredient)]
resolve = resolve' . sortOn (length . snd)
  where
    resolve' []            = []
    resolve' ((al,[]):ls)  = error "Allergen with no mapping"
    resolve' ((al,[i]):ls) = (al,i) : resolve' (map (second (delete i)) ls)
    resolve' ((al,xs):ls)  = error "Allergen with multi mapping"

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  rules <- input fp
  let (ic, am) = construct rules
  print $ HM.toList am
  let amUnique = resolve $ HM.toList am
  print $ map snd $ sortOn fst $ amUnique

main :: IO ()
main = do
  putStr "Filepath: "
  fp <- getLine
  assignment1 fp
  assignment2 fp
