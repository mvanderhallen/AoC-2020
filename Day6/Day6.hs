module Day6 where

  import Data.List ((\\), nub, intersect)

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  assignment1 = do
    ip <- input
    let f = map (length . (\x -> x \\ " ") . nub) ip
    print f
    print $ sum f

  assignment2 = do
    ip <- input
    let answers = map (foldr1 intersect . words) ip
    print answers
    print $ sum $ map length answers
