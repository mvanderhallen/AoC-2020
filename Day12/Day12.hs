-- module Day11 where

-- import Data.List.Index
-- import qualified Data.IntMap.Strict as IM

input :: FilePath -> IO [String]
input = fmap (lines) . readFile

updateCoord :: (Int, Int, Int) -> String -> (Int, Int, Int)
updateCoord (x,y,a) (c:cs) = let d = read cs in
  case c of
  'N' -> (x,y+d,a)
  'E' -> (x+d,y,a)
  'S' -> (x,y-d,a)
  'W' -> (x-d,y,a)
  'L' -> (x,y,(a+d)`mod`360)
  'R' -> (x,y,(a-d)`mod` 360)
  'F' -> case a of
    0   -> updateCoord (x,y,a) ('E':cs)
    90  -> updateCoord (x,y,a) ('N':cs)
    180 -> updateCoord (x,y,a) ('W':cs)
    270 -> updateCoord (x,y,a) ('S':cs)

endCoord :: [String] -> (Int,Int,Int)
endCoord = foldl updateCoord (0,0,0)

computeDistance :: (Int, Int, Int) -> Int
computeDistance (x,y,_) = abs x + abs y

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  ip <- input fp
  print $ computeDistance $ endCoord ip

updateState :: ((Int,Int,Int), (Int, Int)) -> String -> ((Int,Int,Int), (Int, Int))
updateState (w@(x,y,a), s@(sx, sy)) (c:cs) = let d = read cs in
  case c of
    'N' -> ((x,y+d,a), s)
    'E' -> ((x+d,y,a), s)
    'S' -> ((x,y-d,a), s)
    'W' -> ((x-d,y,a), s)
    'L' -> case d `mod` 360 of
      0  -> (w,s)
      90 -> ((-y,x,a),s)
      180 -> ((-x,-y,a),s)
      270 -> ((y,-x,a),s)
    'R' -> updateState (w,s) ('L':show (360-d))
    'F' -> ((x,y,a), (sx+d*x, sy+d*y))

endCoord' :: [String] -> ((Int,Int,Int), (Int, Int))
endCoord' = foldl updateState ((10,1,0),(0,0))

computeDistance' :: (Int, Int) -> Int
computeDistance' (x,y) = abs x + abs y

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  ip <- input fp
  print $ computeDistance' $ snd $ endCoord' ip

main :: IO ()
main = do
  -- fp <- getLine
  let fp = "input.txt"
  assignment1 fp
  assignment2 fp
