module Day4 where
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (void)
  import Data.List (nub)
  import Data.Char (isDigit, isHexDigit)
  import Control.Arrow (first)

  data Field = BYR Int | IYR Int | EYR Int | HGT String | HCL String | ECL String | PID String | CID String deriving (Show, Eq)

  newtype Person = MkPerson [Field] deriving (Show)

  occurs :: Field -> [Field] -> Bool
  occurs f = any (>< f)
    where
      (><) :: Field -> Field -> Bool
      BYR _ >< BYR _ = True
      IYR _ >< IYR _ = True
      EYR _ >< EYR _ = True
      HGT _ >< HGT _ = True
      HCL _ >< HCL _ = True
      ECL _ >< ECL _ = True
      PID _ >< PID _ = True
      CID _ >< CID _ = True
      _     >< _     = False

  {-
  data Person m = MkPerson
    {
      byr :: m Int
    , iyr :: m Int
    , eyr :: m Int
    , hgt :: m Int
    , hcl :: m String
    , ecl :: m String
    , pid :: m Integer
    , cid :: m Int
    }
  -}

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  pString :: Parser String
  pString = many1 (alphaNum <|> char '#')

  fieldSeparator :: Parser ()
  fieldSeparator = void $ try $ oneOf " \n\r\t" <* notFollowedBy (oneOf "\n\r")

  byr :: Parser Field
  byr = (BYR . fromIntegral) <$> (try (symbol "byr:") *> decimal) <?> "BYR"

  iyr :: Parser Field
  iyr = (IYR . fromIntegral) <$> (try (symbol "iyr:") *> decimal) <?> "IYR"

  eyr :: Parser Field
  eyr = (EYR . fromIntegral) <$> (try (symbol "eyr:") *> decimal) <?> "EYR"

  hgt :: Parser Field
  hgt = HGT <$> (try (symbol "hgt:") *> pString) <?> "HGT"

  hcl :: Parser Field
  hcl = HCL <$> (try (symbol "hcl:") *> pString) <?> "HCL"

  ecl :: Parser Field
  ecl = ECL <$> (try (symbol "ecl:") *> pString) <?> "ECL"

  pid :: Parser Field
  pid = PID <$> (try (symbol "pid:") *> pString) <?> "PID"

  cid :: Parser Field
  cid = CID <$> (try (symbol "cid:") *> pString) <?> "CID"

  -- sepEndBy because it can possibly *end* on a single empty line.
  pPerson :: Parser Person
  pPerson = MkPerson <$> try (choice ([byr,iyr,eyr,hgt,hcl,ecl,pid,cid]) `sepEndBy1` fieldSeparator) <?> "Person"

  pPersons :: Parser [Person]
  pPersons = pPerson `sepBy` (symbol "\n\n") -- HACKY!

  isValid :: Person -> Bool
  isValid (MkPerson fields) =
    all (flip occurs fields) [BYR undefined, IYR undefined, EYR undefined, HGT undefined, HCL undefined, ECL undefined, PID undefined] &&
    length fields >= 7 &&
    length fields <= 8

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  assignment1 :: IO ()
  assignment1 = do
    persons <- getAOCInput pPersons "input.txt"
    -- putStrLn $ unlines $ map show persons
    print $ length $ filter isValid persons

-- PART 2

  validField :: Field -> Bool
  validField (BYR i) = i >= 1920 && i <= 2002
  validField (IYR i) = i >= 2010 && i <= 2020
  validField (EYR i) = i >= 2020 && i <= 2030
  validField (HGT h) = case m of
    "cm" -> v >= 150 && v <= 193
    "in" -> v >= 59 && v <= 76
    otherwise -> False
    where
      (v,m) = first read $ span isDigit h
  validField (HCL s) = case s of
    '#':cl -> length cl == 6 && all isHexDigit cl
    otherwise -> False
  validField (ECL s) = s `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
  validField (PID s) = length s == 9 && all isDigit s
  validField (CID _) = True

  isValid' :: Person -> Bool
  isValid' p@(MkPerson fields) = isValid p
    && all validField fields

  assignment2 :: IO ()
  assignment2 = do
    persons <- getAOCInput pPersons "input.txt"
    -- putStrLn $ unlines $ map show persons
    print $ length $ filter isValid' persons
