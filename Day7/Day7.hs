module Day7 where

import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Data.Maybe
import qualified Data.HashMap.Strict as HM
import Debug.Trace
import Control.Monad (join)
import qualified Control.Monad.State as S

data BagReq = MkBagReq Int String

type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace

pString :: Parser String
pString = lexeme $ many1 letter

pEnclosure :: Parser String
pEnclosure = unwords <$> manyTill pString (try (string "bags") <|> try (string "bag"))

(.:) = (.) . (.)
infixr 9 .:

pContained :: Parser _
pContained =
  return Nothing <* string "no other bags" <|>
  Just .: MkBagReq <$> (fromIntegral <$> natural) <*> (unwords <$> manyTill pString (try (string "bags") <|> try (string "bag")))

pRule :: Parser _
pRule = lexeme $ do
  enclosure <- lexeme pEnclosure
  lexeme $ string "contain"
  contained <- pContained `sepBy1` (lexeme $ char ',')
  char '.'
  return (enclosure, sequence contained)

pRules :: Parser _
pRules = many1 pRule

input :: FilePath -> IO _
input fp = do
  input <- readFile fp
  case parse pRules fp input of
    Left err -> do
      print err
      error "parse error"
    Right f ->
      return f

containsShinyGold :: [(String, Maybe [BagReq])] -> String -> S.State ([(String, Bool)]) Bool
containsShinyGold rules s = do
  memo <- S.gets (lookup s)
  case memo of
    Just x ->
      return x
    Nothing ->do
      let contained = join $ lookup s rules
      case contained of
        Nothing -> do
          S.modify ((s,False):)
          return False
        Just c -> do
          case any isShinyGold c of
            True -> do
              S.modify ((s,True):)
              return True
            False -> or <$> mapM (containsShinyGold rules . bagName) c

isShinyGold :: BagReq -> Bool
isShinyGold (MkBagReq _ s)  = s == "shiny gold"

bagName :: BagReq -> String
bagName (MkBagReq _ s) = s

bagCard :: BagReq -> Int
bagCard (MkBagReq i _) = i

assignment1 fp = do
  rules <- input fp
  let (a,s) = S.runState (mapM (containsShinyGold rules) ([r | (r,_) <- rules])) []
  print a
  putStrLn $ show $ length $ filter id a
  print s

shinyGoldContains :: [(String, Maybe [BagReq])] -> String -> S.State ([(String, Int)]) Int
shinyGoldContains rules s = do
  memo <- S.gets (lookup s)
  case memo of
    Just x ->
      return x
    Nothing -> do
      let contained = join $ lookup s rules
      case contained of
        Nothing -> do
          S.modify ((s,0):)
          return 0
        Just c -> do
          recurs <- mapM (shinyGoldContains rules . bagName) c
          let recurs' = sum $ zipWith (*) (map bagCard c) recurs
          let result = recurs' + sum (map bagCard c)
          S.modify ((s,result):)
          return result


          -- return result


assignment2 fp = do
  rules <- input fp
  let (a,s) = S.runState (shinyGoldContains rules "shiny gold") []
  print a
  print s
