{-# LANGUAGE BangPatterns #-}

module Day9 where

import Data.List

input :: FilePath -> IO [Integer]
input fp = (map read . lines) <$> readFile fp

preamble :: Int -> [Integer] -> [Integer]
preamble = reverse .: take

(.:) = (.) . (.)
infixr 9 .:

checkValid :: [Integer] -> [Integer] -> Int -> (Int, Integer)
checkValid (x:xs) previous (!i) =
  if null [ y| (y:ys) <- tails previous, (x-y) `elem` ys ]
  then
    (i,x)
  else
    checkValid xs (take (length previous) (x:previous)) (i+1)

assignment1 fp pr = do
  ip <- input fp
  let pre = preamble pr ip
  print $ checkValid (drop pr ip) pre pr

findWindow :: [Integer] -> [Integer] -> Integer -> [Integer]
findWindow [] window target = if sum window == target then window else []
findWindow (x:xs) window target =
  case compare (sum window) target of
    EQ -> if length window >= 2 then window else findWindow (xs) (x:window) target
    LT -> findWindow xs (x:window) target
    GT -> findWindow (x:xs) (init window) target

assignment2 fp pr = do
  ip <- input fp
  let pre = preamble pr ip
  let target = snd $ checkValid (drop pr ip) pre pr
  let window = findWindow ip [] target
  print window
  let mini = minimum window
  let maxi = maximum window
  print (mini, maxi)
  print $ mini + maxi
