module Giulia where
import System.IO
    ( hClose, openFile, hGetContents, IOMode(ReadMode) )
import Control.Monad ()
import Data.List ()

data Line = Int Int Char String

main :: IO ()
main = do
        handle <- openFile "input.txt" ReadMode
        contents <- hGetContents handle
        let list = lines contents
        let num = map correctPwd list
        print num
        hClose handle

f :: [String] -> [Int]
f = map read


correctPwd :: String -> String
correctPwd line
    | length f >= min && length f <= head max = ""
    | otherwise = line
    where
        f = stringCount myString c
        (min:max) = getNums line
        myString = last . words $ line
        c = head . head $ tail (words line)

stringCount :: (Num a1, Enum a1, Eq a2) => [a2] -> a2 -> [a1]
stringCount str ch = [ y | (x, y) <- zip str [0..], x == ch ]

getNums :: String -> [Int]
getNums line = [read n| n <- map (:[]) line, n /=  "-"]
