{-# LANGUAGE RecordWildCards #-}

module Day2 where
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)

  data Policy = MkPolicy { minV :: Integer, maxV :: Integer, char :: Char} deriving (Show, Eq)
  type Password = String


  pPolicy :: Parsec String () Policy
  pPolicy = do
    minV <- P.natural lexer -- read <$> many1 digit :: Parser Int
    P.symbol lexer "-"
    maxV <- P.natural lexer-- read <$> many1 digit :: Parser Int
    P.whiteSpace lexer
    char <- letter
    return $ MkPolicy minV maxV char

  lexer       = P.makeTokenParser haskellDef

  pPassword :: Parsec String () Password
  pPassword = many1 letter

  pLine :: Parsec String () (Policy, Password)
  pLine = (,) <$> pPolicy <* P.symbol lexer ":" <* P.whiteSpace lexer <*> pPassword

  pFile :: Parsec String () [(Policy, Password)]
  pFile = pLine `endBy` (P.whiteSpace lexer <|> eof)

  checkValid :: Policy -> Password -> Bool
  checkValid (MkPolicy{..}) = between . fromIntegral . length . filter ((==) char)
    where between x = x >= minV && x <= maxV

  assignment :: (Policy -> Password -> Bool) ->  IO ()
  assignment f = do
    input <- readFile "input.txt"
    case runParser pFile () "input.txt" input of
      Left err -> print err
      Right passwords -> do
        putStrLn $ show $ length $ filter (uncurry f) passwords

  assignment1 :: IO ()
  assignment1 = assignment checkValid

  assignment2 :: IO ()
  assignment2 = assignment checkValid'
    where
      checkValid' (MkPolicy{..}) pw = (==1) $ length $ filter ((==) char) $ [pw!!fromIntegral (minV-1), pw!!fromIntegral (maxV-1)]

  input = readFile "input.txt"
