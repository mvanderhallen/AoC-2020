-- module Day11 where

import Data.List.Index
import qualified Data.IntMap.Strict as IM

data Seat = Occupied | Unoccupied | Floor
  deriving (Eq, Enum)

instance Show Seat where
  show Floor = "."
  show Occupied = "#"
  show Unoccupied = "L"

toSeat :: Char -> Seat
toSeat '.' = Floor
toSeat 'L' = Unoccupied
toSeat '#' = Occupied
toSeat _   = error "Unknown seat type"

input :: FilePath -> IO [[Seat]]
input = fmap (map (map toSeat) . lines) . readFile

linearize :: [[Seat]] -> [(Int, Seat)]
linearize m = iconcatMap (\h' -> imap (\w' e-> (h*h'+w', e))) m
  where
    h = length m
    w = length (head m)

toIntMap:: [[Seat]] -> IM.IntMap Seat
toIntMap = IM.fromList . linearize

to2D :: Int -> Int -> (Int, Int)
to2D h x = x `quotRem` h

from2D :: Int -> (Int, Int) -> Int
from2D h (h', w') = h'*h+w'

neighbours :: Int -> Int -> Int -> [Int]
neighbours h w i = [h''*h + w'' | h''<-[h'-1..h'+1], w''<-[w'-1..w'+1], h'' >= 0, h'' < h, w'' >= 0, w'' < w, h'' /= h' || w'' /= w']
  where
    (h', w') = i `quotRem` h

update :: Int -> Int -> IM.IntMap Seat -> IM.IntMap Seat
update h w m = IM.mapWithKey update' m
  where
    update' :: Int -> Seat -> Seat
    update' _ Floor    = Floor
    update' i Unoccupied = if null $ filter ((== Occupied)) $ map (m IM.!) $ neighbours h w i then Occupied else Unoccupied
    update' i Occupied = if (length $ filter ((==Occupied)) $ map (m IM.!) $ neighbours h w i) >= 4 then Unoccupied else Occupied

endState :: (IM.IntMap Seat -> IM.IntMap Seat) -> IM.IntMap Seat -> IM.IntMap Seat
endState f m = if m' == m
  then
    m'
  else
    endState f m'
  where
    m' = f m

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  ip <- input fp
  let h = length ip
  let w = length (head ip)
  let im = toIntMap ip
  print $ length $ filter ((==Occupied) . snd) $ IM.toList $ endState (update h w) im

-- neighbours'' h w m = neighbours' h w m

-- neighbours' :: Int -> Int -> IM.IntMap Seat -> Int -> [Int]
neighbours' h w m i = concatMap (take 1) [up,down,left,right,a1, a2, a3, a4] --concatMap (take 1) [up, down, left, right, a1, a2, a3, a4]
  where
    (h', w') = i `quotRem` h
    from2D' = from2D h
    up    = [from2D' (h'', w')  | h'' <- [h'-1, h'-2..0], m IM.! (from2D' (h'',w')) /= Floor]
    down  = [from2D' (h'', w')  | h'' <- [h'+1, h'+2..h-1], m IM.! (from2D' (h'',w')) /= Floor]
    left  = [from2D' (h',  w'') | w'' <- [w'-1, w'-2..0], m IM.! (from2D' (h',w'')) /= Floor]
    right = [from2D' (h',  w'') | w'' <- [w'+1, w'+2..w-1], m IM.! (from2D' (h',w'')) /= Floor]
    a1    = [from2D' (h'-i,  w'-i) | i <- [1..h], h'-i >= 0, w'-i >= 0, m IM.! (from2D' (h'-i,w'-i)) /= Floor]
    a2    = [from2D' (h'-i,  w'+i) | i <- [1..h], h'-i >= 0, w'+i < w, m IM.! (from2D' (h'-i,w'+i)) /= Floor]
    a3    = [from2D' (h'+i,  w'-i) | i <- [1..h], h'+i < h, w'-i >= 0, m IM.! (from2D' (h'+i,w'-i)) /= Floor]
    a4    = [from2D' (h'+i,  w'+i) | i <- [1..h], h'+i < h, w'+i < w, m IM.! (from2D' (h'+i,w'+i)) /= Floor]

update2 :: (Int -> [Int]) -> IM.IntMap Seat -> IM.IntMap Seat
update2 nb m = IM.mapWithKey update' m
  where
    update' :: Int -> Seat -> Seat
    update' _ Floor    = Floor
    update' i Unoccupied = if null $ filter ((== Occupied)) $ map (m IM.!) $ nb i then Occupied else Unoccupied
    update' i Occupied = if (length $ filter ((==Occupied)) $ map (m IM.!) $ nb i) >= 5 then Unoccupied else Occupied

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  ip <- input fp
  let h = length ip
  let w = length (head ip)
  let im = toIntMap ip
  let nb = neighbours' h w im
  print $ length $ filter ((==Occupied) . snd) $ IM.toList $ endState (update2 nb) im

main :: IO ()
main = do
  -- fp <- getLine
  let fp = "input.txt"
  assignment1 fp
  assignment2 fp
