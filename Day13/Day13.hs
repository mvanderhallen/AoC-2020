
import Data.List

goal :: Integer
goal = 1002632

input :: String -> IO [(Integer, Integer)]
input fp = (sortOn (fst) . busIds . words) <$> readFile fp

busIds :: [String] -> [(Integer, Integer)]
busIds ws = snd $ foldl go (0,[]) ws
  where
    go (delay, bus) "x" = (delay+1, bus)
    go (delay, bus) c   = let b = read c in (delay+1, (delay `mod` b, b) : bus)


crt :: (Integral a, Foldable t) => t (a, a) -> (a, a)
crt = foldr go (0, 1)
    where
    go (r1, m1) (r2, m2) = (r `mod` m, m)
        where
        r = r2 + m2 * (r1 - r2) * (m2 `inv` m1)
        m = m2 * m1

    -- Modular Inverse
    a `inv` m = let (_, i, _) = gcd a m in i `mod` m

    -- Extended Euclidean Algorithm
    gcd 0 b = (b, 0, 1)
    gcd a b = (g, t - (b `div` a) * s, s)
        where (g, s, t) = gcd (b `mod` a) a
