module Day3 where

type Tree = Bool
type Coordinate = (Int, Int)
type Slope = (Int, Int)

input :: FilePath -> IO [[Tree]]
input fp = (map (map toBool) . lines) <$> readFile fp

coordinates :: Coordinate -> Slope -> [Coordinate]
coordinates start (xD, yD) = coordinates'
  where coordinates' = start : map (\(x,y) -> (x+xD,y+yD)) coordinates'

toBool :: Char -> Tree
toBool '#' = True
toBool _   = False

countTrees :: [[Tree]] -> Slope -> Int
countTrees trees slope = length [(x,y) | (x,y) <- coos, (trees!!y)!!x]
  where
    height = length trees
    width  = length $ head trees
    coos   = map (\(x,y) -> (x `mod` width, y)) $ takeWhile ((<height) . snd) $ coordinates (0,0) slope

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  inp <- input fp
  print $ countTrees inp (3,1)

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  inp <- input fp
  print $ product $ map (countTrees inp) [(1,1), (3,1), (5,1), (7,1), (1,2)]

printTest :: [[Bool]] -> IO ()
printTest kaart = do
  putStrLn $ unlines $ map (map (\c -> if c then '#' else '.')) kaart
