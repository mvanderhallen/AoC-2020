{-# LANGUAGE RecordWildCards #-}

import Data.Bits

import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Data.IntMap (IntMap, insert, toList, empty)

type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace

input fp = do
  ip <- readFile fp
  return ip

data Unmasked
data Masked

newtype Mask = MkMask [(Char, Int)]  deriving (Show, Eq)
newtype Memory = MkMem (IntMap Integer)
data Instr a = MkInstr { memAddr :: Integer, memVal :: Integer } deriving (Show, Eq)

pMask :: Parser Mask
pMask = lexeme $ do
  lexeme $ try $ string "mask = "
  bits <- many1 (oneOf "X10")
  let changes = (reverse bits) `zip` [(0::Int) ..]
  return $ MkMask $ changes --

pInstr :: Parser (Instr Unmasked)
pInstr = lexeme $ do
  try $ string "mem["
  addr <- natural
  string "] = "
  val <- natural
  return $ MkInstr addr val

pFile :: Parser [Either Mask (Instr Unmasked)]
pFile = many1 (choice [Left <$> pMask, Right <$> pInstr])

unMemory :: Memory -> IntMap Integer
unMemory (MkMem m) = m

sumMemory :: Memory -> Integer
sumMemory = sum . map snd . toList . unMemory

performInstr :: Memory -> (Instr Masked) -> Memory
performInstr (MkMem m) (MkInstr{..}) = MkMem $ insert (fromIntegral memAddr) memVal m

---------------- ASSIGNMENT 1 --------------------
assignment1 fp = do
  ip <- input fp
  case parse pFile fp ip of
    Left err -> print err
    Right v  -> print $ sumMemory $ emulate (MkMem empty) v

applyMask :: Mask -> Integer -> Integer
applyMask (MkMask f) = \sv -> foldl (\acc (c,p) -> if c == '1' then setBit acc p else clearBit acc p) sv changes
  where
    changes = filter ((/= 'X') . fst) f

applyMaskInstr :: Mask -> (Instr Unmasked) -> (Instr Masked)
applyMaskInstr m (MkInstr{..}) = MkInstr (memAddr) (applyMask m $ memVal)

emulate :: Memory -> [Either Mask (Instr Unmasked)] -> Memory
emulate mem instr = snd $ foldl go (undefined, mem) instr
  where
    go :: (Mask, Memory) -> Either Mask (Instr Unmasked) -> (Mask, Memory)
    go (_, v) (Left m)  = (m, v)
    go (m, v) (Right a) = (m, performInstr v $ applyMaskInstr m a)

-----------------Assignment 2 ----------------------
applyMask' :: Mask -> Integer -> [Integer]
applyMask' (MkMask f) s = foldl applyX [applyOnes] floating
  where
    applyOnes = foldl setBit s $ map snd $ filter ((== '1') . fst) f
    floating = map snd $ filter ((=='X') . fst) f
    applyX vs b = concatMap (\v-> [setBit v b, clearBit v b]) vs

applyMaskInstr' :: Mask -> (Instr Unmasked) -> [(Instr Masked)]
applyMaskInstr' m (MkInstr {..}) = map (flip MkInstr memVal) $ applyMask' m memAddr

emulate' :: Memory -> [Either Mask (Instr Unmasked)] -> Memory
emulate' mem instr = snd $ foldl go (undefined, mem) instr
  where
    go :: (Mask, Memory) -> Either Mask (Instr Unmasked) -> (Mask, Memory)
    go (_, v) (Left m)  = (m, v)
    go (m, v) (Right a) = (m, foldl performInstr v $ applyMaskInstr' m a)

assignment2 fp = do
  ip <- input fp
  case parse pFile fp ip of
    Left err -> print err
    Right v  -> print $ sumMemory $ emulate' (MkMem empty) v

main = do
  fp <- getLine
  assignment1 fp
  assignment2 fp
