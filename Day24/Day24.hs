import Text.Parsec (many1, sepEndBy1, Parsec, parse, string, choice, try)
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)

import Data.List (sort, group, (\\), replicate)
import Data.Bifunctor (bimap)
import Control.Arrow ((&&&), second, first)

import System.IO (hFlush, stdout)


type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace
parens = P.parens lexer

pStep :: Parser [Direction]
pStep = do
  many1 $ choice [ pure E <* string "e", pure W <* string "w"
        , pure SE <* try (string "se"), pure SW <* try (string "sw")
        , pure NE <* try (string "ne"), pure NW <* try (string "nw")]

pSteps :: Parser [[Direction]]
pSteps = pStep `sepEndBy1` whiteSpace

data Direction = E | W | NE | NW | SE | SW deriving (Eq, Show)

toCoord :: [Direction] -> (Int, Int)
toCoord = foldl (flip stepDir) (0,0)

stepDir :: Direction -> (Int, Int) -> (Int, Int)
stepDir E  = first (subtract 1) -- (x-1,y)
stepDir W  = first (+1) -- (x+1,y)
stepDir NE = bimap (subtract 1) (subtract 1) -- (x-1,y-1)
stepDir NW = second (subtract 1) -- (x,y-1)
stepDir SE = second (+1) -- (x,y+1)
stepDir SW = bimap (+1) (+1) -- (x+1,y+1)

input :: FilePath -> IO [[Direction]]
input fp = do
  ip <- readFile fp
  case parse pSteps fp ip of
    Left err -> error $ show err
    Right dirs -> return dirs

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  dirs <- input fp
  print $ length $ filter (odd . length) $ group $ sort $ map toCoord dirs

-------------- ASSIGNMENT 2 --------------

days :: Int -> [(Int, Int)] -> [(Int, Int)]
days n blacks = foldr ($) blacks $ replicate n single

single :: [(Int,Int)] -> [(Int, Int)]
single = uncurry (++) . bimap (stayBlack) (toBlack) . (id &&& id)

stayBlack :: [(Int,Int)] -> [(Int, Int)]
stayBlack blacks = filter go blacks
  where
    go :: (Int, Int) -> Bool
    go = cond . length . filter (`elem` blacks) . neighbours
    cond :: Int -> Bool
    cond n
     | n == 0         = False
     | n > 0 && n < 3 = True
     | n > 2          = False

neighbours :: (Int,Int) -> [(Int,Int)]
neighbours (x,y) = map (flip stepDir (x,y)) [E,W,NW,NE,SW,SE]

toBlack :: [(Int,Int)] -> [(Int, Int)]
toBlack blacks = (\\ blacks) $ map head $ filter ((==) 2 . length) $ group $ sort $ concatMap neighbours blacks

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  dirs <- input fp
  let blacks = map head $ filter (odd . length) $ group $ sort $ map toCoord dirs
  print $ length $ days 100 blacks

-------------- Main --------------

main :: IO ()
main = do
  putStr "Filepath: "
  hFlush stdout
  fp <- getLine
  assignment1 fp
  assignment2 fp
