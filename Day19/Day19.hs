import Data.IntMap (IntMap, insert, empty, (!)) --(map, foldl, filter)
import Data.List hiding (insert)
import Control.Arrow (first, second)
import Control.Monad (sequence)

import Debug.Trace

-- input :: IO
input fp = do
  ip <- readFile fp
  return ip

fullmap' :: [String] -> IntMap [String]
fullmap' rules = foldl insertRule (empty) rules
  where
    fullmap = fullmap' rules
    insertRule :: IntMap [String] -> String -> IntMap [String]
    insertRule im rule = insert ruleNb strs im
      where
        (ruleNb, rule') = second (drop 2) $ first read $ break (== ':') rule :: (Int, String)
        orrule = " | " `isInfixOf` rule'
        lit    = "\"" `isInfixOf` rule'
        rule'' = words rule'
        strs :: [String]
        strs =
          if orrule then do
            let (r1, r2) = break (== '|') rule'
            -- traceM $ (show $ words r1) ++ " && " ++ show (words r2)
            map concat $ (sequence [ fullmap! (read i) | i <- words r1]) ++ (sequence [ fullmap! (read i) | i <- words r2, i/= "|" ])
            -- let ws = [ read i | i <- rule'', i/= "|"] :: [Int] -- trace (show i ++ " [1]\n") $
            -- traceM $ show ws
            -- [a ++ b | a<-fullmap!(ws!!0), b<-fullmap!(ws!!1)] ++ [c ++ d | c<-fullmap!(ws!!2), d<-fullmap!(ws!!3)]
            -- return [(ws!!0 ++ ws !! 1),  (ws!!2 ++ ws!!3)]
            -- return $ concatMap (map (fullmap!) . (read::Int)) [[rule''!!0, rule''!!1], [rule''!!2, rule''!!3]]
          else if lit then
            return $ read $ rule'
          else do
            let ws = [ fullmap! (read i) | i <- rule''] :: [[String]] -- trace (show i ++ " [2]\n") $
            map concat $ sequence ws


assignment1 fp fp2 = do
  rules <- lines <$> input fp
  ws <- lines <$> input fp2
  let corr_words = (fullmap' rules!0)
  print $ length $ filter (`elem` corr_words) ws

assignment2 fp fp2 = do
    rules <- lines <$> input fp
    ws <- fmap lines $ input fp2
    let matches = map (split' (fullmap' rules!42, fullmap' rules!31)) ws
    -- print $ valid' $ matches!!0
    print $ length $ filter (valid') matches
    -- print $ length ws
    -- print $ length $ (fullmap' rules!0) !! 23809
    -- print $ length $ head (fullmap' rules!42) -- `intersect` (fullmap' rules!31)

valid' :: Maybe [Int] -> Bool
valid' Nothing   = False
valid' (Just is) = trace (show (thirtyones >= 1)) $ trace (show is) $ if (not invalid) && thirtyones >= 1 && (fortytwos - thirtyones) > 0 then True else False
  where
    groups = group is
    invalid = case length groups of
      2 -> if (head (groups!!0)) /= 42  || (head (groups!!1)) /= 31 then True else False
      _ -> True
    fortytwos = length $ head groups
    thirtyones = if invalid then 0 else length $ groups!!1

split' :: ([String], [String]) -> String -> Maybe [Int]
split' _ "" = Just []
split' (s42, s31) s
 | length s >= 8 = (\(x,y) -> (:) <$> x <*> y) $ first match $ second (split' (s42,s31)) $ splitAt 8 s
 | length s < 8 = Nothing
  where
    match :: String -> Maybe Int
    match ss = case ss `elem` s42 of
      True -> Just 42
      False -> if ss `elem` s31 then
          Just 31
        else
          Nothing
-- testMatch :: ([String], [String]) -> String -> Bool
-- testMatch (s42, s11) s =
