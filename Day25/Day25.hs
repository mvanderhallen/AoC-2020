{-# LANGUAGE BangPatterns #-}

import System.IO (hFlush, stdout)
import Data.List (lookup)

transformWith :: Int -> [Int]
transformWith (!v) = go'
  where
    go' = v : map next go'
    next !x = let r = (x*v) `mod` 20201227 in seq r r

assignment1 :: (Int,Int) -> IO ()
assignment1 (x,y) = do
  let cardloop = fst $ head $ filter ((== x) . snd) $ zip [1..] $ transformWith 7
  putStr "Card loop number:"
  print cardloop
  putStr "Secret key:"
  print $ modExp y cardloop 20201227

modExp :: Int -> Int -> Int -> Int
modExp x 0 m  = 1
modExp x n m
  | n > m     = modExp x (n `mod` m) m
  | otherwise = if (n `mod` 2) == 0 then bin^2 `mod` m else  (bin^2 `mod` m)*x `mod` m
      where
        bin = (modExp x (n `div` 2) m)

-------------- ASSIGNMENT 2 --------------

assignment2 :: IO ()
assignment2 = putStrLn "whelp dat was ez - Tom"

-------------- Main --------------

main :: IO ()
main = do
  assignment1 (6929599,2448427)
  assignment2
