{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}
-- {-# LANGUAGE UndecidableInstances #-}


import Data.Hashable
import Data.Hashable.Generic
import GHC.Generics
import qualified Data.HashSet as HS
import Data.List (sort, foldl')

import Debug.Trace

input :: FilePath -> IO [Coord]
input fp = do
  ip <- readFile fp
  let board =  concat $ map (uncurry parse) $ zip [0..] $ lines ip
  return board

newtype Coord = MkCoord (Int,Int,Int,Int)
 deriving (Show) via (Int,Int,Int,Int)
 deriving newtype (Eq, Hashable, Ord)

mkCoord :: Int -> Int -> Int -> Int -> Coord
mkCoord x y z d = MkCoord (x,y,z,d)


parse :: Int -> String -> [Coord]
parse i s = foldl go [] $ zip [0..] s
  where
    go ls (j, '.') = ls
    go ls (j, '#') = (mkCoord i j 0 0):ls

cycle :: HS.HashSet Coord -> Coord -> HS.HashSet Coord
cycle hs (MkCoord (w,h,d,_)) =  foldr updateCoord hs coords
  where
    coords :: [Coord]
    coords = [MkCoord (x,y,z,0) |x<- [-w .. w], y <- [-h .. h], z <- [-d .. d]]
    updateCoord :: Coord -> HS.HashSet Coord -> HS.HashSet Coord
    updateCoord pos hs'
      | pos `HS.member` hs = let na = length $ filter (`HS.member` hs) (neighbours3D pos) in (if (na > 1 && na  < 4) then hs' else HS.delete pos hs') -- `trace` ("pos: " ++ show pos ++ ", na" ++ show na)$
      | otherwise                    = let na = length $ filter (`HS.member` hs) (neighbours3D pos) in if na /= 3 then hs' else HS.insert pos hs'


neighbours3D :: Coord -> [Coord]
neighbours3D (MkCoord (x,y,z,_)) = [MkCoord (x',y',z',0) |x'<-[x-1..x+1], y' <- [y-1..y+1], z'<-[z-1..z+1], (x' /= x) || (y' /= y) || (z'/=z)]

cycles :: HS.HashSet Coord -> Int -> Coord -> HS.HashSet Coord
cycles hs i maxC = foldr ($) hs $ (replicate i (flip Main.cycle maxC))

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  ip <- input fp
  print $ HS.size $ cycles (HS.fromList ip) 6 $ MkCoord (15,15,7,0)

--------- ASSIGNMENT 2 ---------

cycle' :: HS.HashSet Coord -> Coord -> HS.HashSet Coord
cycle' hs (MkCoord (w,h,d,hc)) =  foldr updateCoord hs coords
  where
    coords :: [Coord]
    coords = [ MkCoord (x,y,z,a) |x<- [-w .. w], y <- [-h .. h], z <- [-d .. d], a <- [-hc .. hc]]
    updateCoord :: Coord -> HS.HashSet Coord -> HS.HashSet Coord
    updateCoord pos hs'
      | pos `HS.member` hs = let na = length $ filter (`HS.member` hs) (neighbours4D pos) in (if (na > 1 && na  < 4) then hs' else HS.delete pos hs') -- `trace` ("pos: " ++ show pos ++ ", na" ++ show na)$
      | otherwise          = let na = length $ filter (`HS.member` hs) (neighbours4D pos) in if na /= 3 then hs' else HS.insert pos hs'

neighbours4D :: Coord -> [Coord]
neighbours4D (MkCoord (x,y,z,a)) = [MkCoord (x',y',z',a') |x'<-[x-1..x+1], y' <- [y-1..y+1], z'<-[z-1..z+1], a'<-[a-1..a+1], (x' /= x) || (y' /= y) || (z'/=z) || (a'/=a)]

cycles' :: HS.HashSet Coord -> Int -> Coord -> HS.HashSet Coord
cycles' hs i (MkCoord (x,y,z,a)) =
  foldl (\acc f -> f acc) hs $ map (\i -> flip Main.cycle' (MkCoord (x+i, y+i,z+i,a+i))) [1..i]

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  ip <- input fp
  print $ HS.size $ cycles' (HS.fromList ip) 6 $ MkCoord (8,8,0,0)

main :: IO ()
main = do
  fp <- getLine
  assignment1 fp
  assignment2 fp
