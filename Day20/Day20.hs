{-# LANGUAGE RecordWildCards #-}

import Text.Parsec hiding (State)
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Control.Monad (replicateM)
import Data.Maybe (fromMaybe)
import Data.List (transpose, intersect, nub)

import Control.Monad.State
import Control.Arrow

data Dir = N | E | S | W deriving (Eq, Ord, Show)

oppositeDir :: Dir -> Dir
oppositeDir N = S
oppositeDir E = W
oppositeDir S = N
oppositeDir W = E

data Tile = MkTile { tileId :: Int, borders :: [(Int,Dir)], tile:: String}

instance Eq Tile where
  (MkTile tId1 _ _) == (MkTile tId2 _ _ ) = tId1 == tId2

instance Show Tile where
  show (MkTile{..}) = ("Tile: " ++ show tileId) --unlines [("Tile: " ++ show tileId), ("Borders: " ++ show borders), tile]

printReduced :: Tile -> String
printReduced (MkTile{..}) = unlines [("Tile: " ++ show tileId), ("Borders: " ++ show borders)]

printFull :: Tile -> String
printFull (MkTile{..}) = unlines [("Tile: " ++ show tileId), ("Borders: " ++ show borders), tile]

body :: Tile -> [String]
body (MkTile{..}) = map (init . drop 1) $ init $ tail $ lines tile

mkTile :: Int -> [String] -> Tile
mkTile i s = MkTile i (zipWith (min) borders bordersR) (unlines s)
  where
    borders  = zipWith (flip (,)) [N,E,S,W] $ map (toInt) [head s, last $ transpose s, last s, head $ transpose s]
    bordersR = zipWith (flip (,)) [N,E,S,W] $ map (toInt . reverse) [head s, last $ transpose s, last s, head $ transpose s]

borderInts :: Tile -> [Int]
borderInts = map fst . borders
toInt s = foldl (\acc x -> acc*base + (val x)) 0 s
  where
    val '.' = 0
    val '#' = 1
    base = 2

type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace
parens = P.parens lexer

pTile :: Parser Tile
pTile = lexeme $ do
  lexeme $ string "Tile"
  tileId <- natural
  lexeme $ symbol ":"
  tile <- replicateM 10 parseLine
  return $ mkTile (fromIntegral tileId) tile
  where
    parseLine = lexeme $ replicateM 10 (oneOf ".#")

input :: FilePath -> IO [Tile]
input fp = do
  ip <- readFile fp
  case parse (many1 pTile) "" ip of
    Left err -> error $ show err
    Right tiles -> return tiles

assignment1 :: FilePath -> IO ()
assignment1 fp = do
  tiles <- input fp
  print $ product $ map (tileId) $ corners tiles

corners :: [Tile] -> [Tile]
corners tiles = map fst $ filter (((==) 2) . length . snd) $ neighbours tiles

neighbours :: [Tile] -> [(Tile, [(Int,Tile)])]
neighbours tiles =
  [ (currTile, nbsOfTile currTile) | currTile <- tiles ]
  where
    nbsOfTile :: Tile -> [(Int,Tile)]
    nbsOfTile currTile = [ (edge currTile otherTile, otherTile) | otherTile <- tiles, currTile /= otherTile, borderInts otherTile `intersect` borderInts currTile /= [] ]
    edge :: Tile -> Tile -> Int
    edge t1 t2 = case borderInts t2 `intersect` borderInts t1 of
      []  -> error "Edge of non bordering tiles"
      [i] -> i
      xs  -> error "Too many borders"

--------------------- ASSIGNMENT 2 ---------------------

type Used = [Int]
type NbState a = State ([(Tile, [(Int, Tile)])],Used) a

adjOf :: Tile -> Int -> NbState (Maybe (Tile, Int))
adjOf tile i = do
  nbs <- fst <$> get
  return $ do
    neighbours <- lookup tile nbs
    selected   <- lookup i neighbours
    return (selected, fromMaybe 0 $ getOpposite selected i)

getOpposite :: Tile -> Int -> Maybe Int
getOpposite (MkTile{..}) i = do
  dir <- oppositeDir <$> lookup i borders
  lookup dir $ map (\(x,y) -> (y,x)) borders

columnOf :: Tile -> Int -> NbState [Tile]
columnOf tile i = (tile:) <$> do
  modify (second (i:))
  adj <- adjOf tile i
  case adj of
    Nothing      -> return []
    Just (t, mi) -> columnOf (normalize tile t S) mi

normalize :: Tile -> Tile -> Dir -> Tile
normalize t1 t2 E = head [ t2' | t2' <- alternativeTiles t2, edge == edge2 t2']
  where
    edge :: String
    edge = map last $ lines $ tile t1 :: String
    edge2 :: Tile -> String
    edge2 = map head . lines . tile
normalize t1 t2 S = head [ t2' | t2' <- alternativeTiles t2, edge == edge2 t2']
  where
    edge :: String
    edge = last $ lines $ tile t1 :: String
    edge2 :: Tile -> String
    edge2 = head . lines . tile

rowOf :: Tile -> NbState [Tile]
rowOf tile = do
  nbs <- fst <$> get
  used <- snd <$> get
  case lookup tile nbs of
    Nothing -> error "err1"
    Just tiles -> rowOf' tile $ head $ [ ii | (ii, _) <-tiles, not (ii `elem` used)]
  where
    rowOf' :: Tile -> Int -> NbState [Tile]
    rowOf' tile i = (tile:) <$> do
      modify (second (i:))
      adj <- adjOf tile i
      case adj of
        Nothing      -> return []
        Just (t, mi) -> rowOf' (normalize tile t E) mi

reconstructPuzzle :: Tile -> Int -> NbState [[Tile]]
reconstructPuzzle tile i = do
  column <- columnOf tile i
  mapM rowOf column

toGrid :: [[Tile]] -> [String]
toGrid = concatMap gridRow
  where
    gridRow :: [Tile] -> [String]
    gridRow = foldl (\acc t -> zipWith (++) acc $ body t) (repeat "")

alternativeTiles :: Tile -> [Tile]
alternativeTiles (MkTile{..}) = map (mkTile tileId) $ alternatives $ lines tile

assignment2 :: FilePath -> IO ()
assignment2 fp = do
  tiles <- input fp
  let nbs = neighbours tiles
  let corner = corners tiles !! 0
  let Just [i, j] = map fst <$> lookup corner nbs
  let lu = rotateToTopLeft corner i j
  let puzzle = evalState (reconstructPuzzle lu i) $ (neighbours tiles, [])
  let rawPuzzle = toGrid puzzle
  let actualMap = head $ filter ((>0) . seaMonsters) $ alternatives rawPuzzle
  let nbMonsters = seaMonsters actualMap
  print $ (length $ filter (=='#') $ concat actualMap) - (nbMonsters * 15)

--rotateToTopLeft takes a tile, the direction that should be south and the direction that should be east
rotateToTopLeft :: Tile -> Int -> Int -> Tile
rotateToTopLeft t south east = head $ filter go $ alternativeTiles t
  where
    go tile = and [(lookup south $ borders tile) == Just S, (lookup east $ borders tile) == Just E]

alternatives :: [String] -> [[String]]
alternatives t = concatMap (\x -> [x, flip' x]) go
  where
    go :: [[String]]
    go = take 4 $ t : map rotate go
    rotate = map reverse . transpose
    flip' = map reverse

seaMonsters :: [String] -> Int
seaMonsters []     = 0
seaMonsters puzzle = length $ [j | i<-[1..lineLim], j<-[0..lineLength ], monsterAt j (puzzle!!(i-1)) (puzzle!!i) (puzzle!!(i+1))]
  where
  lineLim :: Int
  lineLim = (length puzzle)-2
  lineLength :: Int
  lineLength = (length $ head puzzle)-20
  monsterAt :: Int -> String -> String -> String -> Bool
  monsterAt j s1 s2 s3 = all (=='#') $ concat [map ((s1!!) . (j+)) [18], map ((s2!!) . (j+)) [0,5,6,11,12,17,18,19], map ((s3!!) . (j+)) [1,4,7,10,13,16]]

main :: IO ()
main = do
  putStr "Filepath: "
  fp <- getLine
  assignment1 fp
  assignment2 fp
