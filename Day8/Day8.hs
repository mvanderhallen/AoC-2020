module Day8 where

{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DerivingVia #-}

import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Data.Maybe
import qualified Data.HashMap.Strict as HM
import Debug.Trace
import Control.Monad (join)
import qualified Control.Monad.State as S
import Data.List

type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace
integer = P.integer lexer

data Command = ACC Int | NOP Int | JUMP Int deriving (Show, Eq, Ord)
newtype Counter = MkPC Int deriving (Show) deriving (Enum, Ord, Eq, Num) via Int
newtype Acc  = MkAcc Int deriving (Show) deriving (Ord, Eq, Num) via Int

-- deriving via (Int) instance Enum Counter

mkPC :: Integral a => a -> Counter
mkPC = MkPC . fromIntegral

mkAcc :: Integral a => a -> Acc
mkAcc = MkAcc . fromIntegral

offset :: Integral a => Counter -> a -> Counter
offset (MkPC x) = mkPC . (x+) . fromIntegral

parseCommand :: Parser Command
parseCommand =  (ACC . fromIntegral <$> (lexeme (string "acc") *> integer))
            <|> (NOP . fromIntegral <$> (lexeme (string "nop") *> integer))
            <|> (JUMP . fromIntegral <$> (lexeme (string "jmp") *> integer))

parseCommands :: Parser [Command]
parseCommands = many1 (lexeme parseCommand)

input :: FilePath -> IO [Command]
input fp = do
  ip <- readFile fp
  case parse parseCommands fp ip of
    Left err -> do
      print err
      error "parse error"
    Right f -> do
      return f

process :: [Command] -> Acc -> [Counter] -> Counter -> Int
process cmds acc'@(MkAcc acc) ctrs ctr
  | ctr `elem` ctrs = acc
  | otherwise = case lookup ctr (zip [mkPC x | x<-[0..]] cmds) of
      Just (ACC x)  -> process cmds (MkAcc (acc+x)) (ctr:ctrs) (offset ctr 1)
      Just (NOP x)  -> process cmds acc' (ctr:ctrs) (offset ctr 1)
      Just (JUMP x) -> process cmds acc' (ctr:ctrs) (offset ctr x)
      Nothing       -> error $ unwords ["Program counter out of bounds:", show ctr]

assignment1 fp = do
  ip <- input fp
  print $ process ip (MkAcc 0) [] (mkPC 0)

checkIfTerminates :: [Command] -> [Counter] -> Counter -> Bool
checkIfTerminates cmds ctrs ctr
  | ctr `elem` ctrs = False
  | otherwise = case lookup ctr (zip [mkPC 0..] cmds) of
      Just (ACC _)  -> checkIfTerminates cmds (ctr:ctrs) (offset ctr 1)
      Just (NOP _)  -> checkIfTerminates cmds (ctr:ctrs) (offset ctr 1)
      Just (JUMP x) -> checkIfTerminates cmds (ctr:ctrs) (offset ctr x)
      Nothing       -> True


process' :: [Command] -> Acc -> Bool -> [Counter] -> Counter -> Int
process' cmds acc@(MkAcc acc') s ctrs ctr
  | ctr `elem` ctrs = error $ "Error! Loop:\n" ++ show ctrs
  | otherwise = case lookup ctr (zip [mkPC 0..] cmds) of
      Just (ACC x) -> process' cmds (mkAcc $ acc'+x) s (ctr:ctrs) (offset ctr 1)
      Just (NOP x) -> if s &&  checkIfTerminates cmds (ctr:ctrs) (offset ctr x)
        then process' cmds acc False (ctr:ctrs) (offset ctr x)
        else process' cmds acc s     (ctr:ctrs) (offset ctr 1)
      Just (JUMP x) -> if s && checkIfTerminates cmds (ctr:ctrs) (offset ctr 1)
        then process' cmds acc False (ctr:ctrs) (offset ctr 1)
        else process' cmds acc s     (ctr:ctrs) (offset ctr x)
      Nothing       -> acc'


assignment2 fp = do
  ip <- input fp
  print $ process' ip (MkAcc 0) True [] (mkPC 0)
