import Prelude hiding (take)
import Data.Sequence (Seq (..), (|>), fromList, take)
import Text.Parsec (many1, many, sepBy1, Parsec, parse, char, letter, string)
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Data.Foldable (toList)
import Control.Arrow ((***))
import Control.Monad.State

import qualified Data.HashSet as HS

type Parser a = Parsec String () a
lexer = P.makeTokenParser haskellDef
lexeme = P.lexeme lexer
symbol = P.symbol lexer
natural = P.natural lexer
decimal = P.decimal lexer
whiteSpace = P.whiteSpace lexer
whitespace = whiteSpace
parens = P.parens lexer

pPlayer :: Parser [Int]
pPlayer = lexeme $ do
  string "Player " <* natural <* symbol ":"
  many1 (fromIntegral <$> natural)

pFile :: Parser ([Int], [Int])
pFile = lexeme $ (,) <$> pPlayer <*> pPlayer

input :: FilePath -> IO ([Int],[Int])
input fp = do
  ip <- readFile fp
  case parse pFile fp ip of
    (Left err) ->  error $ show err
    (Right res) -> return res

combat :: ([Int],[Int]) -> [Int]
combat = toList . combat' . (fromList *** fromList)
  where
    combat' :: (Seq Int, Seq Int) -> Seq Int
    combat' (Empty, x)       = x
    combat' (x, Empty)       = x
    combat' (a:<|as, b:<|bs)
      | a > b = combat' (as|>a|>b, bs)
      | otherwise = combat' (as, bs|>b|>a)

assignment1 :: FilePath -> IO ()
assignment1 fp =
  input fp >>= (return . score . combat) >>= print

score :: [Int] -> Int
score = sum . zipWith (*) [1 ..] . reverse

------------------- ASSIGNMENT 2 -------------------

data Player = P1 | P2 deriving (Eq, Show, Ord, Enum)

recursiveCombat :: ([Int], [Int]) -> [Int]
recursiveCombat = toList . snd . (`evalState` HS.empty) . recursiveCombat' . (fromList *** fromList)
  where
  recursiveCombat' :: (Seq Int, Seq Int) -> State (HS.HashSet ([Int], [Int])) (Player, Seq Int)
  recursiveCombat' seqs = do
    endPlay <- gets (((toList *** toList) seqs) `HS.member`)
    modify (HS.insert $ (toList *** toList) seqs)
    if endPlay then
      return (P1, fst seqs)
    else
      case seqs of
        (Empty, x) -> return (P2, x)
        (x, Empty) -> return (P1, x)
        (a:<|as, b:<|bs) ->
          if (length as >= a) &&  (length bs >= b) then
            case fst $ evalState (recursiveCombat' (take a as, take b bs)) (HS.empty) of
              P1 -> recursiveCombat' (as|>a|>b, bs)
              P2 -> recursiveCombat' (as, bs|>b|>a)
          else
            if a > b then
              recursiveCombat' (as|>a|>b, bs)
            else
              recursiveCombat' (as,bs|>b|>a)

assignment2 :: FilePath -> IO ()
assignment2 fp =
  --fmap (score . recursiveCombat) (input fp) >>= print
  input fp >>= (return . score . recursiveCombat) >>= print

main :: IO ()
main = do
  putStr "Filepath: "
  fp <- getLine
  assignment1 fp
  assignment2 fp
